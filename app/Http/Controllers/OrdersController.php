<?php
namespace App\Http\Controllers;
use App\Models\Orders;
use App\Models\order_items;
use App\Models\users;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function orderSearchIdUpdate(Request $request)
    {
        $order = Orders::where('id', $request->input('id'))->get();
        foreach($order as $item){
            $item->order_status =  $request->input('order_status');
            $item->save();
        }
        return response()->json(['message' => 'success', 'status' => 200 ,'user' => $order ]);
    }
    public function index()
    {
        return Orders::all();
    }
    public function getOrderItems()
    {
        return order_items::all();
    }
    public function statusCount()
    {
        $success = 100;
        $cancel = 10;
        $test = 20;
        $successCount = 0;
        $cancelCount = 0;
        $testCount = 0;

        $orderList = Orders::all();
        foreach($orderList as $item){
          if($item->order_status === $success){
            $successCount ++ ;
            }
            elseif($item->order_status === $cancel){
                $cancelCount ++ ;
            }
            elseif($item->order_status === $test){
                $testCount ++ ;
            }
        }
        return response()->json(array('status' => 'success', 'successCount' => $successCount, 'cancelCount' => $cancelCount, 'testCount' => $testCount));
    }
    public function saveOrderData(Request $request)
    {
        $Orders = new Orders();
        $Orders->order_status = $request->input('order_status');
        $Orders->created_at =  date("Y-m-d H:i:s");
        $Orders->updated_at =  date("Y-m-d H:i:s");
        $Orders->menu_total = $request->input('menu_total');
        $Orders->customer_note = $request->input('customer_note');
        $Orders->phone = $request->input('phone');
        $Orders->save();
        $order_items = $request->input('order_items');
        foreach ($order_items as $item) {
            $order_item = new order_items();
            $order_item->order_id = $Orders->id;
            $order_item->total_price = $item['total_price'];
            $order_item->item_price = $item['productPrice'];
            $order_item->item_count = $item['productQuantity'];
            $order_item->created_at =  date("Y-m-d H:i:s");
            $order_item->updated_at =  date("Y-m-d H:i:s");
            $order_item->name_mon =$item['productName'];
            $order_item->name_eng = $item['productName'];
            $order_item->save();
        }
        return response()->json(array('status' => 'success', 'data' => $Orders, 'order-items' => $order_item, 'message' => 'Success'));
    }
    public function addUser(Request $request)
    {
        // return $request->input('user_name');
        $user = new users();
        $user->firstname = $request->input('firstname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->password = $request->input('password');
        $user->created_at = date("Y-m-d");
        $user->save();
        return response()->json(['message' => 'success', 'status' => 200 ,'user' => $user ]);
    }
    public function loginUser(Request $request)
    {
        $status = 0;
        $userInfo=null;
        $articles = users::where('email', $request->input('user_name'))->get();
        foreach($articles as $item){
            if($item->password === $request->input('password')){
                $status ++ ;
                $userInfo = $item;
            }else{
                $status = 0 ;
            }
        }
        if($status > 0){
            return response()->json(['message' => 'success', 'status' => $status,'userInfo' => $userInfo, 'Login Success' ]);
        }else if ($status === 0){
            return response()->json(['message' => 'failed', 'status' => $status, 'Invalid password and email' ]);
        }

    }
}
